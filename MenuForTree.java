/*
 * Gina Bettger and Ainslie (Bellarmine University CS 215)
 * Create a menu that will create a tree from text file, 
 * insert and/or delete nodes, search and give level information, and write the data 
 * to a file. There are a maximum of five levels in the tree (for a lay person user, 
 * we numbered the layers 1-5 instead of 0-4.) Using linked list.
 */

import java.util.*;
import java.io.*;
public class MenuForTree {
    
        static List L1 = new LinkedList();
        static List L2 = new LinkedList();
        static List L3 = new LinkedList();
        static List L4 = new LinkedList();
        static List L5 = new LinkedList();
        //there will be a maximum of five levels in this tree. 
        
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        String element = "";
        int insertLevel =0;
        int levelCount = 1;        
        
        System.out.println("Tree Operations Menu");
        
        char ch=' '; //will be used to determine if do while should continue or not
        do{
        System.out.println("1. Read data from file");
        System.out.println("2. Insert node");
        System.out.println("3. Delete node");
        System.out.println("4. Search for node");
        System.out.println("5. Print data to file");
        
        System.out.println("\nPlease enter a number from the menu: ");
        int choice = scanner.nextInt(); //choice is used in a switch case menu. 
        
        switch(choice){
            case 1:
                //menu item 1 will read from the file to populate the lists (and tree)
                System.out.println("Reading from file...");
                readTree(levelCount);
                System.out.println(L1);
                System.out.println(L2);
                System.out.println(L3);
                System.out.println(L4);
                System.out.println(L5);
                //printing the tree values to check to make sure everything read properly
                //these values displayed L or R based on sequence order in next code 
                //(levels will have LRLRLRL pattern for those read from file. User can choose to display inputs L or R
                break;
            case 2:
                //menu item 2 - insert an item. choose the level, element, and L or R
                System.out.println("Enter level that you want to add a child to: (1-5)");
                insertLevel = scanner.nextInt();
                System.out.println("Enter element to insert: ");
                element = scanner.next();
                System.out.println("Enter L to display it left, enter R to display it right");
                char lr = scanner.next().charAt(0);
                if(lr=='L'||lr=='l'){
                   element=element.concat("L");}
                if(lr=='R'||lr=='r'){
                    element=element.concat("R");}
                System.out.println(element);
                switch(insertLevel){
                    case 1:
                        L1.add(element);
                        break;
                    case 2:
                        L2.add(element);
                        break;
                    case 3:
                        L3.add(element);
                        break;
                    case 4:
                        L4.add(element);
                        break;
                    case 5:
                        L5.add(element);
                        break;
                }
                break;
            case 3:
                System.out.println("Enter element to delete: ");
                element=scanner.next();
                System.out.println(element);
                String elementWithR = element.concat("R");
                String elementWithL = element.concat("L");
                //will allow for search and delete of element with or without L/R at end
                L1.remove(element);
                L1.remove(elementWithR);
                L1.remove(elementWithL);
                L2.remove(element);
                L2.remove(elementWithR);
                L2.remove(elementWithL);
                L3.remove(element);
                L3.remove(elementWithR);
                L3.remove(elementWithL);
                L4.remove(element);
                L4.remove(elementWithR);
                L4.remove(elementWithL);
                L5.remove(element);
                L5.remove(elementWithR);
                L5.remove(elementWithL);
                break;
            case 4:
                System.out.println("Enter element to search: ");
                element = scanner.next();
                //searching for element with or without directional letter
                String elementNoDir = element.substring(0,(element.length()-1));
                //System.out.println(elementNoDir);
                if(L1.contains(element)==true||L1.contains(elementNoDir)==true){
                    System.out.println(element +" found at level 1");
                }
                else if(L2.contains(element)==true||L2.contains(elementNoDir)==true){
                    System.out.println(element +" found at level 2");
                }
                else if(L3.contains(element)==true||L3.contains(elementNoDir)==true){
                    System.out.println(element +" found at level 3");
                }
                else if(L4.contains(element)==true||L4.contains(elementNoDir)==true){
                    System.out.println(element +" found at level 4");
                }
                else if(L5.contains(element)==true||L5.contains(elementNoDir)==true){
                    System.out.println(element +" found at level 5");
                }
                else{
                    System.out.println(element +" was not found.");
                }
                break;
                //also indicates the level the element (node) was found at 
            case 5: 
                System.out.println("Printing to file....");
                //print to text file!
                writeTree();
                break;
            default:
                System.out.println("Wrong entry.");
                break;
        }
        
        System.out.println("\nDo you want to continue? (Type y or n)");
        ch = scanner.next().charAt(0);
        }while(ch=='Y' || ch=='y');
    }
    
    public static int readTree(int levels){
        String fileName = "C:\\Documents\\data structures\\nary_tree_input.txt";
        //will need to be changed for other computers/users
        try {
            FileReader inputFile= new FileReader(fileName);
            BufferedReader bufferReader = new BufferedReader(inputFile);
            String line;

            while((line=bufferReader.readLine())!=null){ //This statement reads each line
                //System.out.println(line); //checks to make sure line is read correctly.
                //reads in line by line, then based on the current line/level 
                //assigns each word/element/node to the appropriate list. 
                
                if(levels==1){
                    L1.add(line);
                }
                
                else if(levels==2){
                    String strline="";
                    String rest="";
                    for(int x=0; x < line.length(); x++){
                        if(line.charAt(x) == ' '){
                            strline = line.substring(0, x);
                            rest = line.substring(x + 1, line.length());
                            L2.add(strline);
                            line = rest;
                            x=0;
                        }
                        if(x==(line.length()-1)){
                            L2.add(rest);
                        }
                    }
                    //L1.add(L2);
                }
                
                else if(levels==3){
                    String strline="";
                    String rest="";
                    for(int x=0; x < line.length(); x++){
                        if(line.charAt(x) == ' '){
                            strline = line.substring(0, x);
                            rest = line.substring(x + 1, line.length());
                            L3.add(strline);
                            line = rest;
                            x=0;
                        }
                        if(x==(line.length()-1)){
                            L3.add(rest);
                        }
                    }
                    //L2.add(L3);
                }
                
                else if (levels==4){
                    String strline="";
                    String rest="";
                    for(int x=0; x < line.length(); x++){
                        if(line.charAt(x) == ' '){
                            strline = line.substring(0, x);
                            rest = line.substring(x + 1, line.length());
                            L4.add(strline);
                            line = rest;
                            x=0;
                        }
                        if(x==(line.length()-1)){
                            L4.add(rest);
                        }
                    }
                    //L3.add(L4);
                }
                
                else if(levels==5){
                    String strline="";
                    String rest="";
                    for(int x=0; x < line.length(); x++){
                        if(line.charAt(x) == ' '){
                            strline = line.substring(0, x);
                            rest = line.substring(x + 1, line.length());
                            L5.add(strline);
                            line = rest;
                            x=0;
                        }
                        if(x==(line.length()-1)){
                            L5.add(rest);
                        }
                    }
                    //L4.add(L5);
                }
                else{
                    System.out.println("Too many levels");
                }
                levels++;
            }
        }catch (IOException | NumberFormatException e) {
            System.out.println("Error while reading file line by line:" +e.getMessage());
        }
    return levels;    
    }
    
    public static void writeTree(){  //writes tree data in file line by line (level by level)
        String fileWrite ="C:\\Documents\\data structures\\outputTree.txt";         
        //address and name will need to be changed based on user/computer.
        try{
        FileWriter fileWriter=new FileWriter(fileWrite);
            
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        
        bufferedWriter.write(L1.toString());
        bufferedWriter.newLine();
        bufferedWriter.write(L2.toString());
        bufferedWriter.newLine();
        bufferedWriter.write(L3.toString());
        bufferedWriter.newLine();
        bufferedWriter.write(L4.toString());
        bufferedWriter.newLine();
        bufferedWriter.write(L5.toString());
            
        bufferedWriter.newLine();
        bufferedWriter.close();
        }catch(IOException ex){
                System.out.println("Error writing to file");
        }
    }
}
